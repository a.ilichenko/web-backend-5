<!DOCTYPE html>
<html lang="ru">


  <head>
      <meta charset="utf-8"/>
      <title>Backend-5</title>
       <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
       <link href="style-login.css" rel="stylesheet">
       <link href="https://faviconka.ru/ico/1/faviconka.ru_1_25078.ico" rel="icon">
  </head>

  <body>
    <div class="container-lg px-0">

    <div class="main  row mx-auto">
    <section id="form">
    <h2>Авторизация</h2>


<?php


header('Content-Type: text/html; charset=UTF-8');

session_start();

if (!empty($_SESSION['login'])) {
  session_destroy();
  header('Location: ./');
}


if ($_SERVER['REQUEST_METHOD'] == 'GET') {

?>

  <div class="login-page">
        <div class="form">
          <form class="login-form" action="" method="post">
            <input class="input-field" name="login"  placeholder="логин"/>
            <input class="input-field" name="pass" placeholder="пароль"/>
            <input class="gradient-button" type="submit" value="войти">
          </form>
        </div>
  </div>


<?php
}

else {
  $user = 'u24412';
  $pass = '3582090';
  $db = new PDO('mysql:host=localhost; dbname=u24412', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

  $stmt1 = $db->prepare('SELECT  user_id, hash_pass FROM form WHERE login = ?');
  $stmt1->execute([$_POST['login']]);

  $row = $stmt1->fetch(PDO::FETCH_ASSOC);
  if (!$row) {
    header('Location: ?nologin=1');
    exit();
  }
  
  $pass_hash = substr(hash("sha256", $_POST['pass']), 0, 20);
  if ($row['hash_pass'] != $pass_hash) {
    header('Location: ?wrongpass=1');
    exit();
  }

  $_SESSION['login'] = $_POST['login'];
  $_SESSION['uid'] = $row['user_id'];

  header('Location: ./');
}
?>

</section>
</div>
</body>
</html>
